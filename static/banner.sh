#!/usr/bin/env bash
if [[ -f ./colors.sh && -f ./banner.sh ]]; then
    source ./colors.sh
    source ./banner-settings.sh
fi

echo -e "$([[ -f ./colors.sh && -f ./banner.sh ]])"

AUTHOR="emil.wozniak.591986@gmail.com"

### app banner
export WELCOME_BANNER="${GROUND_BLUE}${W6}${W6} ${BOLD}  Welcome in Terminal Radio  ${RESET}${GROUND_BLUE}${W6}${W6}   ${RESET}
${GROUND_BLUE}${WHITESPACE_LONG}  ${RESET}
${GROUND_BLUE}  thanks for ${CYAN}${BOLD}MPV ©${RESET}${GROUND_BLUE} and ${CYAN}${BOLD}Ansiweather ©${RESET}${GROUND_BLUE} you can listen to ${GROUND_BLUE}  ${RESET}
${GROUND_BLUE}${W6}  ${GREEN}${BOLD}RadioCodeCamp ©${RESET}${GROUND_BLUE} from your Ubuntu terminal${W6}${GROUND_BLUE}  ${RESET}
${GROUND_BLUE} ${W6}developed by ${YELLOW}${AUTHOR}${W6}${GROUND_BLUE}  ${RESET}
${GROUND_BLUE}${WHITESPACE_LONG}  ${RESET}"
