#!/usr/bin/env bash
if [[ -f ./colors.sh && -f ./banner-settings.sh ]]; then
source ./colors.sh
source ./banner-settings.sh
fi

### help string
HELP_STRING="${CYAN}+${SHORT_STROKE_LINE}----${RESET}  Possible options:  ${CYAN}----${SHORT_STROKE_LINE}+${RESET}\n\
${BLUE}${W3}${BOLD}-b${RESET}  hide app banner.sh
${BLUE}${W3}${BOLD}-c${RESET}  for choose radio station
${BLUE}${W3}${BOLD}-cs${RESET} for load saved frequencies.sh and choose frequency
${BLUE}${W3}${BOLD}-s${RESET}  save new frequency ${GROUND_RED}${BLACK}${BOLD} BETA ${RESET}
${BLUE}${W3}${BOLD}-w${RESET}  for ansiweather${W6}${W3}  \n\
${YELLOW}${W3}${W3} -F${RESET} ${BOLD}${YELLOW}Toggle forecast mode for the next five days    ${RESET}
${W3}${W3}${W3} ${BOLD}${YELLOW}(${GREEN}true${YELLOW} or false)${W16}${W16}${W6}${RESET}
${W3}${W3}${W3} ${RED}${BOLD} value true disable all flags without flag ${YELLOW} -l ${RESET}
${YELLOW}${W3}${W3} -l${RESET} Specify location (${BOLD}Warsaw${RESET} or name)
${YELLOW}${W3}${W3} -u${RESET} Specify unit system to use (${BOLD}metric${RESET} or imperial)
${YELLOW}${W3}${W3} -f${RESET} Specified number of upcoming days ( 1 - 7 ${BOLD}[2]${RESET} )
${YELLOW}${W3}${W3} -s${RESET} Toggle symbols display ${boolean}
${YELLOW}${W3}${W3} -i${RESET} Toggle symbols display ${boolean}
${YELLOW}${W3}${W3} -w${RESET} Toggle wind data display ${boolean}
${YELLOW}${W3}${W3} -p${RESET} Toggle pressure data display ${boolean}
${YELLOW}${W3}${W3} -d${RESET} Toggle daylight data display ${boolean}

      EXAMPLE SYNTAX: ${BOLD}${BLUE}-s -w ${YELLOW} -F -l Berlin -f 2 ${RESET}
"
