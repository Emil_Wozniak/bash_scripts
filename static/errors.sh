#!/usr/bin/env bash
if [[ -f ./colors.sh && -f ./banner-settings.sh ]]; then
source ./colors.sh
source ./banner-settings.sh
fi

### error strings
export MPV_ERROR="${CYAN}|${WHITESPACE_LONG}|\n\
|${RED}${W16}${BOLD}YOU DON'T HAVE INSTALLED MPV${RESET}${W16}${CYAN}|\n\
${CYAN}|${WHITESPACE_LONG}|\n\
|${RESET}${W6}to fix this issue just type and press enter${W6}${CYAN}|\n\
|${GREEN}${W6}${W6}   sudo apt-get install mpv${W6}${W6}    ${CYAN}|\n\
${CYAN}|${WHITESPACE_LONG}|${RESET}"

export ANSI_ERROR="${CYAN}|${RED}${W6}     ${BOLD}YOU DON'T HAVE INSTALLED ANSIWEATHER${RESET}${W6}  ${CYAN}|\n\
${CYAN}|${WHITESPACE_LONG}|\n\
|${RESET}${W6}to fix this issue just type and press enter${W6}${CYAN}|\
\n|${GREEN}${W6}     sudo apt-get install ansiweather${W6}${W6}${CYAN}|"
