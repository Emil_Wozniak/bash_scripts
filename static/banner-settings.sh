#!/usr/bin/env bash
base_dir="$(dirname "$0")"
echo -e "$base_dir"
if [[ -f $base_dir/colors.sh ]]; then
    source $base_dir/colors.sh
fi

export WHITESPACE_LONG="                                                       " # 55 whitespaces
export FINISH_LINE="+-------------------------------------------------------+${RESET}"
export SHORT_STROKE_LINE="-------------" # 13 -
export W16="             "               # 16 whitespaces
export W3="   "
export W6="      "
export boolean="${RESET}(true or ${BOLD}false${RESET})"
