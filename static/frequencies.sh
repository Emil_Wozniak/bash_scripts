#!/usr/bin/env bash
export  CODERADIO_ADMIN_LF=https://coderadio-admin.freecodecamp.org/radio/8010/radio.mp3?1564068287
export  CODERADIO_ADMIN_HF=https://coderadio-admin.freecodecamp.org/radio/8010/low.mp3?1564068287
export  CODERADIO_RELAY_LF=https://coderadio-relay.freecodecamp.org/radio/8010/radio.mp3
export  CODERADIO_RELAY_HF=https://coderadio-relay.freecodecamp.org/radio/8010/low.mp3
