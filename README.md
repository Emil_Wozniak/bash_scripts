# Terminal Radio 

This application was created for listen to FreeCodeCamp Radio from Ubuntu terminal

## Requirement

TerminalRadio requires the fallowing dependencies:
- [MPV](https://github.com/mpv-player/mpv/) 
- [AnsiWeather](https://github.com/fcambus/ansiweather)

## Usage

### Synopsis

```html
terminal-radio.sh   [ -b  ] hide script banner
                    [ -c  ] for choose radio station
                    [ -cs ] for load saved frequencies and choose frequency
                    [ -s  ] save new frequency  BETA 
                    [ -w  ] for ansiweather
                            [ -F ] Toggle forecast mode for the next five days (true or false) default true
                                   value true disable all flags without flag  -l 
                            [ -l ] Specify location (Warsaw or name)                           default Warsaw
                            [ -u ] Specify unit system to use (metric or imperial)             default metric
                            [ -f ] Specified number of upcoming days ( 1 - 7 )                 default 2
                            [ -s ] Toggle symbols display (true or false)                      default false
                            [ -i ] Toggle symbols display (true or false)                      default false
                            [ -w ] Toggle wind data display (true or false)                    default false
                            [ -p ] Toggle pressure data display (true or false)                default false
                            [ -d ] Toggle daylight data display (true or false)                default false
              
example syntax: 

./terminal-radio.sh -b -F -w -l Berlin -f 2 -s -w   
executes without script banner -F disable defaults and display forecast
for 2 days in Berlin with symbols and wind
 
./terminal-radio.sh -b -c
executes hide banner and allows to choose radio frequency

./terminal-radio.sh -b -c -w -F -l Berlin
executes hide banner, then disable weather defaults and display forecast for 3 days in Berlin,
and finally allows you to choose your favourite radio frequency

``` 

## CURRENT ISSUES

`./terminal-radio.sh -w -c` will ignore second flag

## License

TerminalRadio is free open source project
For more information read [licence](LICENCE)

## Author

TerminalRadio is developed by Emil Woźniak
- emil.wozniak.591986@gmail.com
