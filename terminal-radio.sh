#!/usr/bin/env bash
#Author @emil.wozniak.591986@gmail.com

TARGET_FLAGS=$#
AUTHOR="emil.wozniak.591986@gmail.com"
dirname="$(dirname "$0")"

if [[ -d static ]]; then
  source ./static/banner-settings.sh
  source ./static/colors.sh
  source ./static/banner.sh
  source ./static/help-banner.sh
  source ./static/errors.sh
  source ./static/frequencies.sh
fi

######################
## VARIABLES SECTION ##
######################

## OTHER VARIABLES ##
is_banner_flag=true
filename="$dirname/frequencies.sh.txt"
filename_test="$dirname/frequencies_test.txt"
frequencies_colored=()
frequencies_index=()
frequencies=()
values=()

## WEATHER VARIABLES
default=true
location="Warsaw"
days=2
unit="metric"
symbol=false
uv_index=false
wind=false
humidity=true
pressure=false
daylight=false

###############
## FUNCTIONS ##
###############

## checks if any of flags is present
if [[ $# -lt 1 ]]; then
  echo -e "\n${WELCOME_BANNER}\n${HELP_STRING}\n"
  exit 1
fi

function display_banner() {
  if ${is_banner_flag}; then

    echo "${WELCOME_BANNER}"
    # displays if MPV or Ansiweather is not installed
    [[ ! -f /usr/bin/mpv ]] && echo -e "${MPV_ERROR}"
    [[ ! -f /usr/bin/ansiweather ]] && echo -e "${ANSI_ERROR}"
    echo -e "${FINISH_LINE}"
  else
    echo
  fi
}

function display_weather() {
  while [[ -n $2 ]]; do
    case "$2" in
    -l)
      location="$3"
      shift
      ;;
    -f)
      days="$3"
      shift
      ;;
    -u)
      unit="$3"
      shift
      ;;
    -F) default=false ;;
    -s) symbol=true ;;
    -i) uv_index=true ;;
    -w) wind=true ;;
    -p) pressure=true ;;
    -d) daylight=true ;;
    --)
      shift
      break
      ;;
    esac
    shift
  done
  #  echo -e "${default} l=${location} f=${days} h=${humidity} p=${pressure} d=${daylight} i=${uv_index}"

  display_banner
  [[ ! -f /usr/bin/ansiweather ]] && echo -e "${CYAN}${FINISH_LINE}\n${ANSI_ERROR}\n${FINISH_LINE}"
  [[ -f /usr/bin/ansiweather ]] && [[ ${default} == true ]] && ansiweather -F -l ${location}
  [[ -f /usr/bin/ansiweather ]] && [[ ${default} != true ]] && ansiweather -l ${location} -f ${days} -u ${unit} -s ${symbol} -a true -h ${humidity} -p ${pressure} -d ${daylight}
}

#############
# APP LOGIC #
#############
while test $# != 0; do
  ## Get the terminal's dimensions
  height=$(tput lines)
  width=$(tput cols)
  ## Clear the terminal
  clear
  #    ## Set the cursor to the middle of the terminal
  #    echo "$height $width"
  tput cup "$((height / 24))" #"$((width/6))"

  case "$1" in
  -b) # Welcome banner.sh section
    is_banner_flag=false ;;
  # Help section
  -h) echo -e "${HELP_STRING}" ;;

  # Weather section
  -w)
    echo "$(display_weather)"
    shift
    ;;

  # adds new address for -sl flag
  -s)
    regex='(https?|ftp|file):\/\/[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]'
    read -p "Please write server http address: " server_name
    if [[ ${server_name} =~ '(https?|ftp|file):\/\/[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]' ]]; then
      echo "$server_name is valid address"
      echo "$server_name" >>${filename_test}
      echo -e "${GREEN}${BOLD}Address added successfully: ${YELLOW}${server_name}${RESET}"
    else
      echo "${RED}${BOLD}$server_name is not valid address. Operation ended with refuse ${RESET}"
    fi
    ;;

  # FreeCodeCamp radio selection section with read data from external file
  -cs)
    param="$2"
    next=$#
    statusbar=""
    PS3='Please select: '
    display_banner

    if [[ ${param} == "s" ]]; then
      ## MPV VARIABLES
      statusbar="--term-osd-bar"
    fi

    ## handles no mpv error
    if [[ ! -f /usr/bin/mpv ]]; then
      echo -e "${MPV_ERROR}\n${CYAN}${FINISH_LINE}${RESET}"
    else
      echo -e "${CYAN}----------${RESET}  Select a radio station frequency:  ${CYAN}----------${RESET}"
      index=0

      ## Populates the list from external file
      while read line; do
        index=$(($index + 1))
        frequencies+=("${line}")
        frequency="${YELLOW}${line}${RESET}"
        frequencies_colored+=("${frequency}")
        values+=("${index}")
      done <${filename}

      ## adds Quit option
      frequencies_colored+=("${RED}Quit${RESET}")
      end=${#values[@]}
      end=$(($end + 1))
      values+=("${end}")

      ## makes select from options
      select option in "${frequencies_colored[@]}"; do
        ## Populates options
        for ((i = 0; i < ${#values[@]} + 2; ++i)); do
          if [[ ${i} -lt ${#frequencies_colored[@]} ]]; then
            # handles correct option
            if [[ ${frequencies_colored[i]} != ${RED}Quit${RESET} ]]; then
              case ${option} in
              ${frequencies_colored[i]}) mpv ${frequencies[i]} ${statusbar} ;;
              esac
              # handles quit
            elif [[ ${frequencies_colored[i]} == ${RED}Quit${RESET} ]]; then
              echo "${RED}Exiting... (Quit)${RESET}"
              exit 0
            fi
            # handles invalid option
          elif [[ ${i} -gt ${#frequencies_colored[@]} ]]; then
            case ${option} in
            *) echo "invalid option $REPLY" ;;
            esac
          fi
        done
      done
    fi
    shift
    ;;
  -c)
    echo "$(display_banner)"
    if [[ $2 == -w ]]; then
      while [[ -n $2 ]]; do
        case "$2" in
        -l)
          location="$3"
          shift
          ;;
        -f)
          days="$3"
          shift
          ;;
        -u)
          unit="$3"
          shift
          ;;
        -F) default=false ;;
        -s) symbol=true ;;
        -i) uv_index=true ;;
        -w) wind=true ;;
        -p) pressure=true ;;
        -d) daylight=true ;;
        --)
          shift
          break
          ;;
        esac
        shift
      done
      [[ ! -f /usr/bin/ansiweather ]] && echo -e "${CYAN}${FINISH_LINE}\n${ANSI_ERROR}\n${FINISH_LINE}"
      [[ -f /usr/bin/ansiweather ]] && [[ ${default} == true ]] && ansiweather -F -l ${location}
      [[ -f /usr/bin/ansiweather ]] && [[ ${default} != true ]] && ansiweather -l ${location} -f ${days} -u ${unit} -s ${symbol} -a true -h ${humidity} -p ${pressure} -d ${daylight}
    fi
    PS3='Please select: '
    echo -e "${CYAN}----------${RESET}  Select a radio station frequency:  ${CYAN}----------${RESET}"
    if [[ ! -f /usr/bin/mpv ]]; then
      echo -e "${MPV_ERROR}\n${CYAN}${FINISH_LINE}${RESET}"
    else
      options=(
        "${YELLOW}$CODERADIO_ADMIN_LF${RESET}"
        "${YELLOW}$CODERADIO_ADMIN_HF${RESET}"
        "${YELLOW}$CODERADIO_RELAY_LF${RESET}"
        "${YELLOW}$CODERADIO_RELAY_HF${RESET}"
        "${RED}Quit${RESET}"
      )
      select opt in "${options[@]}"; do
        case ${opt} in
        "${YELLOW}$CODERADIO_ADMIN_LF${RESET}") mpv "${CODERADIO_ADMIN_LF}" ;;
        "${YELLOW}$CODERADIO_ADMIN_HF${RESET}") mpv "${CODERADIO_ADMIN_HF}" ;;
        "${YELLOW}$CODERADIO_RELAY_LF${RESET}") mpv "${CODERADIO_RELAY_LF}" ;;
        "${YELLOW}$CODERADIO_RELAY_HF${RESET}") mpv "${CODERADIO_RELAY_HF}" ;;
        "${RED}Quit${RESET}")
          echo "${RED}Exiting... (Quit)${RESET}"
          break
          ;;
        *) echo "invalid option $REPLY" ;;
        esac
      done
    fi
    shift
    ;;
  --)
    shift
    break
    ;;
  esac
  shift
done
num=1
for param in "$@"; do
  echo -e "#$num: $param"
  num=$(($num + 1))
done
